// Import the functions you need from the SDKs you need
import * as firebase from "firebase";
import "firebase/database";

const config = {
  apiKey: "AIzaSyBN2r5N6KZkfh7Dzh6LPDRdPcuzxHlMON8",
  authDomain: "crud-realtime-995f8.firebaseapp.com",
  projectId: "crud-realtime-995f8",
  storageBucket: "crud-realtime-995f8.appspot.com",
  messagingSenderId: "204714902460",
  appId: "1:204714902460:web:ed9afeb85547b58287e1b9"
};

// Initialize Firebase
firebase.initializeApp(config);
export default firebase.database();